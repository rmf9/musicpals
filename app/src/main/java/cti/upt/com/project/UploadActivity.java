package cti.upt.com.project;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;


public class UploadActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    private StorageReference storageRef;
    private static final int READ_REQUEST_CODE = 42;
    private Uri selectedFile = null;
    private EditText description;
    private String instrumentType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_upload );
        FirebaseStorage storage = FirebaseStorage.getInstance();
        storageRef = storage.getReference();
        description = findViewById( R.id.description );
        Spinner spinner = findViewById(R.id.instruments_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.instruments_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                instrumentType = parent.getItemAtPosition( position ).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        } );
    }

    public void clicked(View view) {
        switch (view.getId()) {
            case R.id.button_select:
                performFileSearch();
                break;
            case R.id.button_upload:
                if(selectedFile!=null){
                    upload( selectedFile );
                }else{
                    Toast.makeText(getApplicationContext(), "Select a file first..",
                            Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void upload(Uri uri){
        final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        @SuppressLint("SimpleDateFormat") final String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
        assert currentUser != null;
        StorageReference recordingRef = storageRef.child("audio/"+ currentUser.getUid()+"/"+timestamp+ uri.getLastPathSegment());
        UploadTask uploadTask = recordingRef.putFile( uri );
        Toast.makeText(getApplicationContext(), "Please wait for upload to finish..", Toast.LENGTH_LONG).show();
        uploadTask.addOnFailureListener( new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                Toast.makeText(getApplicationContext(), "Upload unsuccessful. Try again.", Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                //save recording info to the database
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                mDatabase = FirebaseDatabase.getInstance().getReference();
                assert downloadUrl != null;
                Recording newRecording = new Recording(currentUser.getUid(), description.getText().toString() ,downloadUrl.toString(), instrumentType);
                mDatabase.child( "recordings" ).child(timestamp.replace( '.',' ' )).setValue(newRecording);
                startActivity(new Intent(UploadActivity.this, ProfileActivity.class));
            }
        });
    }

    /**
     * Fires an intent to spin up the "file chooser" UI and select a file.
     */
    public void performFileSearch() {
        // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
        // browser.
        @SuppressLint("InlinedApi") Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        // Filter to show only audio, using the image MIME data type.
        intent.setType("audio/*");
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent resultData) {
        // The ACTION_OPEN_DOCUMENT intent was sent with the request code
        // READ_REQUEST_CODE. If the request code seen here doesn't match, it's the
        // response to some other intent, and the code below shouldn't run at all.
        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            // The document selected by the user won't be returned in the intent.
            // Instead, a URI to that document will be contained in the return intent
            // provided to this method as a parameter.
            // Pull that URI using resultData.getData().
            Uri uri;
            if (resultData != null) {
                uri = resultData.getData();
                selectedFile = uri;
            }
        }
    }
}

