package cti.upt.com.project;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SearchActivity extends AppCompatActivity{

    private static final String TAG = "Search:";
    private RecordingRecyclerViewAdapter recordingAdapter;
    private String instrumentType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        RecyclerView rv = findViewById( R.id.resultsList );
        rv.setLayoutManager(new LinearLayoutManager(this));
        this.recordingAdapter = new RecordingRecyclerViewAdapter();
        rv.setAdapter(recordingAdapter);
        Spinner spinner = findViewById(R.id.typeSpinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.instruments_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                instrumentType = parent.getItemAtPosition( position ).toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        } );
    }

    public void clicked(View view) {
        switch (view.getId()) {
            case R.id.profileButton:
                ProfileActivity.currentuid = "default";
                switchActivity(ProfileActivity.class);
                break;
            case R.id.filterB:
                recordingAdapter.stopAudio();
                Toast.makeText( this, "Filtering...", Toast.LENGTH_SHORT).show();
                updateList();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
        recordingAdapter.stopAudio();
    }

    @Override
    public void onStart(){
        super.onStart();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference().child( "recordings" );
        myRef.addValueEventListener( new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    Recording post1 = postSnapshot.getValue(Recording.class);
                    addToRecordings( post1);
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    private void switchActivity(Class activity){
        startActivity(new Intent(this, activity));
    }


    private void removeFromRecordings(final Recording recording){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                recordingAdapter.remove(recording);
            }
        });
    }

    private void addToRecordings(final Recording recording) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                recordingAdapter.addRecording(recording);
            }
        });
    }

    private void updateList(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference().child( "recordings" );
        myRef.addValueEventListener( new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    final Recording post1 = postSnapshot.getValue(Recording.class);
                    if(instrumentType.equals( "Mixed" )) {
                        addToRecordings( post1 );
                        continue;
                    }
                    if (post1 != null) {
                        if(!post1.getInstrument().equals( instrumentType ))
                            removeFromRecordings( post1 );
                        else
                            addToRecordings( post1 );
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

}
