package cti.upt.com.project;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.InputStream;
import java.util.Arrays;

public class ProfileActivity extends AppCompatActivity {

    public static String currentuid;
    private static final String TAG = "Profile:";
    private TextView name;
    private ImageView image;
    private TextView area;
    private RecordingRecyclerViewAdapter recordingAdapter;
    private DatabaseReference mDatabase;
    private StorageReference storageRef;
    private Button recordB;
    private Button signOutB;
    private Button profileB;
    private User viewedUser;
    public static Location currentLocation;
    private static final int READ_REQUEST_CODE = 42;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_profile );
        FirebaseStorage storage = FirebaseStorage.getInstance();
        storageRef = storage.getReference();
        name = findViewById( R.id.textName );
        area = findViewById( R.id.textArea );
        image = findViewById( R.id.profileImage );
        recordB = findViewById( R.id.recordButton );
        signOutB = findViewById( R.id.signOutButton );
        profileB = findViewById( R.id.profileButton );
        RecyclerView rv = findViewById( R.id.listView );
        rv.setLayoutManager( new LinearLayoutManager( this ) );
        this.recordingAdapter = new RecordingRecyclerViewAdapter();
        rv.setAdapter( recordingAdapter );
        setLocation();
    }

    public void clicked(View view) {
        switch (view.getId()) {
            case R.id.searchButton:
                switchActivity( SearchActivity.class );
                break;
            case R.id.profileButton:
                ProfileActivity.currentuid = "default";
                switchActivity( ProfileActivity.class );
                break;
            case R.id.signOutButton:
                FirebaseAuth.getInstance().signOut();
                switchActivity( SearchActivity.class );
                break;
            case R.id.recordButton:
                FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                if (currentUser!=null && currentuid.equals( currentUser.getUid() )) {
                    switchActivity( UploadActivity.class );
                } else {
                    Toast.makeText( getApplicationContext(), viewedUser.getEmail(), Toast.LENGTH_SHORT ).show();
                    Intent emailIntent = new Intent( Intent.ACTION_SENDTO, Uri.fromParts( "mailto", viewedUser.getEmail(), null ) );
                    emailIntent.putExtra( Intent.EXTRA_SUBJECT, "Music Pals" );
                    emailIntent.putExtra( Intent.EXTRA_TEXT, "" );
                    startActivity( Intent.createChooser( emailIntent, "Send email" ) );
                }
                break;
            case R.id.profileImage:
                performFileSearch();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        recordingAdapter.stopAudio();
    }

    private void switchActivity(Class activity) {
        startActivity( new Intent( this, activity ) );
    }

    @Override
    public void onStart() {
        super.onStart();
        handleUser();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult( requestCode, resultCode, data );
        if (requestCode == 123) {
            if (resultCode == RESULT_OK) {
                //save a list of user ids in the database
                mDatabase = FirebaseDatabase.getInstance().getReference();
                final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                double latitude = 0;
                double longitude = 0;
                if(currentLocation!=null){
                    latitude = currentLocation.getLatitude();
                    longitude = currentLocation.getLongitude();
                }
                if(currentUser==null) return;
                final User newUser = new User( currentUser.getUid(), latitude, longitude, currentUser.getDisplayName(), currentUser.getEmail(), currentUser.getPhotoUrl() );
                DatabaseReference myRef = mDatabase.child( "users" ).child( currentUser.getUid() );
                myRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.exists()) {
                            mDatabase.child( "users" ).child( currentUser.getUid() ).setValue( newUser );
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
            return;
        }
        // The ACTION_OPEN_DOCUMENT intent was sent with the request code
        // READ_REQUEST_CODE. If the request code seen here doesn't match, it's the
        // response to some other intent, and the code below shouldn't run at all.
        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            // The document selected by the user won't be returned in the intent.
            // Instead, a URI to that document will be contained in the return intent
            // provided to this method as a parameter.
            // Pull that URI using resultData.getData().
            Uri uri;
            if (data != null) {
                uri = data.getData();
                saveImage( uri );
            }
        }
    }

    private void handleUser() {
        // Check if user is signed in (non-null) and update UI accordingly.
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            // Create and launch sign-in intent
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders( Arrays.asList(
                                    new AuthUI.IdpConfig.Builder( AuthUI.EMAIL_PROVIDER ).build(),
                                    new AuthUI.IdpConfig.Builder( AuthUI.GOOGLE_PROVIDER ).build() ) )
                            .setIsSmartLockEnabled( false )
                            .build(),
                    123 );
        } else {
            setupProfile();
        }
    }

    @SuppressLint("SetTextI18n")
    private void setupProfile() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        recordingAdapter.setProfileActivity();
        if (firebaseUser !=null&&currentuid.equals( "default" )) currentuid = firebaseUser.getUid();

        FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        DatabaseReference myRef1 = database1.getReference().child( "users" );
        myRef1.addValueEventListener( new ValueEventListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot1) {
                for (DataSnapshot postSnapshot1 : dataSnapshot1.getChildren()) {
                    User post1 = postSnapshot1.getValue( User.class );
                    viewedUser = post1;
                    if (post1!=null&&post1.getUid().equals( currentuid )) {
                        String username = post1.getName();
                        name.setText( "Name: " + username );
                        area.setText( "Area: " + post1.getLatitude() + ", " + post1.getLongitude() );
                        new DownloadImageTask(image).execute(post1.getPhotoUrl());
                        //get list of all recordings that belong to current user
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference myRef = database.getReference().child( "recordings" );
                        myRef.addValueEventListener( new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                    Recording post = postSnapshot.getValue( Recording.class );
                                    if (post!=null&&post.getUid().equals( currentuid ))
                                        addToRecordings( post );
                                }
                            }
                            @Override
                            public void onCancelled(DatabaseError error) {
                                Log.w( TAG, "Failed to read value.", error.toException() );
                            }
                        } );
                        break;
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.w( TAG, "Failed to read value.", error.toException() );
            }
        } );
        if (firebaseUser!=null&&currentuid.equals( firebaseUser.getUid() )) {
            recordB.setText( "Add new recording" );
            signOutB.setClickable( true );
            signOutB.setVisibility( View.VISIBLE );
            image.setClickable( true );
        } else {
            recordB.setText( "Email this person" );
            signOutB.setClickable( false );
            signOutB.setVisibility( View.GONE );
            profileB.setBackgroundColor( Color.parseColor("#f26419") );
            image.setClickable( false );
        }
    }

    private void setLocation() {
        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient( this );
        if (ActivityCompat.checkSelfPermission( this, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission( this, android.Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener( this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            currentLocation = location;
                        }
                    }
                } );
    }

    private void addToRecordings(final Recording recording) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                recordingAdapter.addRecording(recording);
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.RGB_565;
                mIcon11 = BitmapFactory.decodeStream(in, null, options);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    /**
     * Fires an intent to spin up the "file chooser" UI and select a file.
     */
    public void performFileSearch() {
        // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
        // browser.
        @SuppressLint("InlinedApi") Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        // Filter to show only images, using the image MIME data type.
        intent.setType("image/*");
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    private void saveImage(Uri uri){
        final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        StorageReference recordingRef = storageRef.child("pictures/"+ uri.getLastPathSegment());
        UploadTask uploadTask = recordingRef.putFile( uri );
        Toast.makeText(getApplicationContext(), "Picture is being saved ...", Toast.LENGTH_LONG).show();
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                Toast.makeText(getApplicationContext(), "Upload unsuccessful. Try again.", Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                //save picture to the database
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                mDatabase = FirebaseDatabase.getInstance().getReference();
                double latitude = 0;
                double longitude = 0;
                if (currentLocation != null) {
                    latitude = currentLocation.getLatitude();
                    longitude = currentLocation.getLongitude();
                }
                User newUser;
                if(currentUser!=null) {
                    newUser = new User( currentUser.getUid(), latitude, longitude, currentUser.getDisplayName(), currentUser.getEmail(), downloadUrl );
                    mDatabase.child( "users" ).child( currentUser.getUid() ).setValue( newUser );
                }
            }
        });
    }
}
