package cti.upt.com.project;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;


class RecordingRecyclerViewAdapter extends RecyclerView.Adapter<RecordingRecyclerViewAdapter.RecordingEntryViewHolder> {

    private final List<Recording> recordings = new LinkedList<>();
    private MediaPlayer mediaPlayer;
    private boolean profileActivity = false;


    RecordingRecyclerViewAdapter() {
    }

    void addRecording(Recording recording) {
        if(recordings.contains( recording )) return;
        recordings.add( 0, recording );
        notifyItemInserted( 0 );
    }

    void setProfileActivity(){
        profileActivity = true;
    }

    @Override
    public RecordingEntryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recording, parent, false);
        return new RecordingEntryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecordingEntryViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final Recording info = recordings.get(position);
        holder.tvContent.setText(info.getName());
        holder.listenB.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mediaPlayer!=null)
                    mediaPlayer.stop();
                String url = info.getDownloadUrl();
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType( AudioManager.STREAM_MUSIC);
                try {
                    mediaPlayer.setDataSource(url);
                    mediaPlayer.prepare(); // might take long! (for buffering, etc)
                    mediaPlayer.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } );
        holder.stopB.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mediaPlayer!=null)
                mediaPlayer.stop();
            }
        } );
        if(profileActivity) holder.profileB.setVisibility( View.GONE );
        else {
            holder.profileB.setVisibility( View.VISIBLE );
            holder.profileB.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Recording current = recordings.get( position );
                    ProfileActivity.currentuid = current.getUid();
                    Intent intent = new Intent( v.getContext(), ProfileActivity.class );
                    v.getContext().startActivity( intent );
                }
            } );
        }

    }

    @Override
    public int getItemCount() {
        return recordings.size();
    }

    class RecordingEntryViewHolder extends RecyclerView.ViewHolder{

        TextView tvContent;
        Button profileB;
        Button listenB;
        Button stopB;

        RecordingEntryViewHolder(View itemView) {
            super(itemView);
            this.tvContent = itemView.findViewById(R.id.tv_recording_item_content);
            this.profileB = itemView.findViewById(R.id.viewButton);
            this.listenB = itemView.findViewById(R.id.listenButton);
            this.stopB = itemView.findViewById( R.id.stopButton );
        }
    }


    void stopAudio(){
        if(mediaPlayer!=null)
            mediaPlayer.stop();
    }

    void remove(Recording recording){
        if(!recordings.contains( recording )) return;
        int position = recordings.indexOf( recording );
        recordings.remove(recording);
        notifyItemRemoved( position );
    }

}
